import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UnitCardComponent } from '../components/unit-card/unit-card.component';
import { HttpClientModule } from '@angular/common/http';
import { GameManagerService } from 'src/services/game-manager.service';
import { GameSaveService } from 'src/services/game-save.service';

@NgModule({
  declarations: [
    AppComponent,
    UnitCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [GameManagerService, GameSaveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
