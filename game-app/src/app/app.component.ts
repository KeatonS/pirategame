import { Component } from '@angular/core';
import { GameManagerService } from 'src/services/game-manager.service';
import { GameSaveService, PlayerSaveData } from 'src/services/game-save.service';
import { Unit } from 'src/models/unit.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'game-app';

  public activePlayerCard: string;
  public saveCards: PlayerSaveData;

  constructor(private gameManagerService: GameManagerService,
    private gameSaveService: GameSaveService) {

    this.gameManagerService.getActivePlayerCard().subscribe(data => {
      this.activePlayerCard = data;
    });

    this.gameSaveService.getPlayerSaveDataSubject().subscribe(data => {
      this.saveCards = data;
      console.log(JSON.stringify(this.saveCards.unitSaves[0].cardId) + "SAVE");
    });
  }

}
